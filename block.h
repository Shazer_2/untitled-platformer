#ifndef BLOCK_H
	#define BLOCK_H

#include "SDL.h"

class Block {
public:
	int x, y, w, h;

	Block::Block(int, int, int, int);
	void draw(SDL_Surface*, int, int, Uint32);
	void draw(SDL_Surface*, int, int, SDL_Surface*);
};

#endif