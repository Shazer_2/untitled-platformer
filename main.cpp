#include "SDL.h"
#include "SDL_image.h"

#include "player.h"
#include "map.h"
#include "input.h"

#include <ctime>
#include <string>
#include <iostream>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

SDL_Surface *screen = NULL;
SDL_Event event;

bool init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		return false;
	}

	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
	if (screen == NULL) {
		return false;
	}

	SDL_WM_SetCaption("Untitled Platformer!", NULL);
	return true;
}

void clean_up() {
	SDL_Quit();
}


int main(int argc, char* args[]) {
	if (init() == false) {
		return 1;
	}

	if (SDL_Flip(screen) == -1) {
		return 1;
	}

	bool quit = false;

	clock_t initial_time = clock();
	SDL_Rect clear = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

	Player player = Player(0, 0, 32, 32);
	vector<Block> blocks = player.map.get_blocks();

	while (quit == false) {
		SDL_FillRect(screen, &clear, 0x000000);

		player.draw(screen, 0xFF0000);

		if ((clock() - initial_time) >= 15) {

			if (player.move_l || player.controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) {
				player.move_left();
			}

			if(player.move_r || player.controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) {
				player.move_right();
			}
			
			if (player.controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK) {
				quit = true;
			}

			player.update(event);
			player.check_all_collisions();

			initial_time = clock();
		}

		for (int block = 0; block < blocks.size(); block++) {
			blocks[block].draw(screen, blocks[block].x*32, blocks[block].y*32, 0xFFFFFF);
		}
		

		while (SDL_PollEvent(&event)) {
			Input input = Input(event);

			switch(input.get_event_type()) {
			case SDL_QUIT:
				quit = true;
				break;
			}

			switch(input.get_key_pressed()) {
			case SDLK_ESCAPE:
				quit = true;
				break;
			case SDLK_LEFT:
				player.move_l = true;
				break;
			case SDLK_RIGHT:
				player.move_r = true;
				break;
			}

			switch(input.get_key_released()) {
			case SDLK_LEFT:
				player.move_l = false;
				break;
			case SDLK_RIGHT:
				player.move_r = false;
				break;
			}
		}

		SDL_Flip(screen);
	}

	delete(player.controller);
	clean_up();
	return 0;
}