#include "SDL.h"
#include "player.h"
#include "map.h"
#include "input.h"
#include "CXBOXController.h"

#include <iostream>

using namespace std;

Player::Player(float x, float y, int w, int h) {
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->map = Map("1.txt");
	
	vy = 0;
	vx = 0;
	acc = 2;
	maxspeed = 10;
	jumping = true;
	double_jumping = false;

	move_l = false;
	move_r = false;

	controller = new CXBOXController(1);
}

void Player::draw(SDL_Surface* screen, Uint32 color) {
	SDL_Rect player_rect = {(int) x, (int) y, w, h};
	SDL_FillRect(screen, &player_rect, color);
}

void Player::update(SDL_Event event) {
	Input input = Input(event);

	vx /= 1.10;

	if (controller->IsConnected()) {
		if (controller->GetState().Gamepad.bLeftTrigger) {
			acc = 1;
			maxspeed = 4;
		} else {
			acc = 2;
			maxspeed = 10;
		}

		if (controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A) {
			jump();
		}
	}

	if (input.get_key_pressed() == SDLK_LSHIFT) {
		acc = 1;
		maxspeed = 4;
	} else if (input.get_key_released() == SDLK_LSHIFT) {
		acc = 2;
		maxspeed = 10;
	}

	if (input.get_key_pressed() == SDLK_UP) {
		jump();
	}

	fall();
	move();
}

void Player::move_left() {
	if (vx > 0) { 
		vx = 0;
	}
	
	if (vx > -maxspeed) { 
		vx -= acc; 
	}
}

void Player::move_right() {
	if (vx < 0) { 
		vx = 0; 
	}
	
	if (vx < maxspeed) { 
		vx += acc; 
	}
}

void Player::jump() {
	if (!jumping) {
		vy = -15;
		jumping = true;
	}
}

void Player::double_jump() {
	vy = -10;
	double_jumping = true;
}

void Player::accelerate() {
	vy += 1;
}

void Player::fall() {
	accelerate();
	y += vy;
}

void Player::move() {
	x += vx;
}

void Player::check_all_collisions() {
	float temp_y = y;

	tuple<bool, int, int> collision_jump;
	tuple<bool, int, int> collision_jump_right;
	
	if (vy < 0) {
		collision_jump = map.is_collision((x+1)/32, y/32);
		collision_jump_right = map.is_collision((x+w-1)/32, y/32);

		if (get<0>(collision_jump)) {
			y = get<2>(collision_jump)*32+h;
		}

		if (get<0>(collision_jump_right)) {
			y = get<2>(collision_jump_right)*32+h;
		}
	}

	tuple<bool, int, int> collision_bottom_left;
	tuple<bool, int, int> collision_bottom_right;

	if (vy > 0) {
		collision_bottom_left = map.is_collision((x+1)/32, (y+h)/32);
		collision_bottom_right = map.is_collision((x+w-1)/32, (y+h)/32);

		if (get<0>(collision_bottom_left)) {
			y = get<2>(collision_bottom_left)*32-h;
		}
			
		if (get<0>(collision_bottom_right)) {
			y = get<2>(collision_bottom_right)*32-h;
		}
	}

	if (vx < 0) {
		tuple<bool, int, int> collision_left = map.is_collision(x/32, y/32);
		tuple<bool, int, int> collision_left_bottom = map.is_collision(x/32, (y+h-1)/32);

		if (get<0>(collision_left)) {
			x = get<1>(collision_left)*32+w;
			vx = 0;
		}

		if (get<0>(collision_left_bottom)) {
			x = get<1>(collision_left_bottom)*32+w;
			vx = 0;
		}
	}

	if (vx > 0) {
		tuple<bool, int, int> collision_right = map.is_collision((x+w)/32, y/32);
		tuple<bool, int, int> collision_right_bottom = map.is_collision((x+w)/32, (y+h-1)/32);
		
		if (get<0>(collision_right)) {
			x = get<1>(collision_right)*32-w;
			vx = 0;
		}

		if (get<0>(collision_right_bottom)) {
			x = get<1>(collision_right_bottom)*32-w;
			vx = 0;
		}
	}

	if (vy < 0) {
		collision_jump = map.is_collision((x+1)/32, (y-h)/32);
		collision_jump_right = map.is_collision((x+w-1)/32, (y-h)/32);
			
		if (get<0>(collision_jump)) {
			vy = 0;
		} else {
			y = temp_y;
		}

		if (get<0>(collision_jump_right)) {
			vy = 0;
		} else {
			y = temp_y;
		}
	}

	if (vy > 0) {
		collision_bottom_left = map.is_collision((x+1)/32, (y+h+1)/32);
		collision_bottom_right = map.is_collision((x+w-1)/32, (y+h+1)/32);

		if (get<0>(collision_bottom_left)) {
			vy = 0;
			jumping = false;
			double_jumping = false;
		} else {
			y = temp_y;
		}
			
		if (get<0>(collision_bottom_right)) {
			vy = 0;
			jumping = false;
			double_jumping = false;
		} else {
			y = temp_y;
		}
	}
}