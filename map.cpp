#include "map.h"
//#include "block.h"

#include <iostream>
#include <fstream>
#include <tuple>

using namespace std;

Map::Map(string filename) {
	map = filename;
}

Map::Map() {

}

vector<Block> Map::get_blocks() {
	string line;
	ifstream map_file;
	vector<string> lines;
	vector<Block> block_positions;

	map_file.open(map);
	if (map_file.is_open()) {
		while (!map_file.eof()) {
			getline(map_file, line);
			lines.push_back(line);
		}
	}

	map_file.close();
	
	int count=0;
	for (int y = 0; y < lines.size(); y++) {
		for (int x = 0; x < lines[y].size(); x++) {
			if (lines[y][x] == 'X') {
				block_positions.push_back(Block::Block(x, y, 32, 32));
			}
		}
	}

	return block_positions;
}

tuple<bool, int, int> Map::is_collision(int x, int y) {
	vector<Block> blocks = this->get_blocks();
	for (int block = 0; block < blocks.size(); block++) {
		if (x == blocks[block].x && y == blocks[block].y) {
			return make_tuple(true, blocks[block].x, blocks[block].y);
		}
	}

	return make_tuple(false, 0, 0);
}