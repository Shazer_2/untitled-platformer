#include "CXBOXController.h"

CXBOXController::CXBOXController(int playerNumber) {
	_controllerNum = playerNumber - 1;
}

XINPUT_STATE CXBOXController::GetState() {
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));
	XInputGetState(_controllerNum, &_controllerState);
	return _controllerState;
}

bool CXBOXController::IsConnected() {
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	DWORD result = XInputGetState(_controllerNum, &_controllerState);
	if (result == ERROR_SUCCESS) {
		return true;
	} else {
		return false;
	}
}

void CXBOXController::Vibrate(int leftVal, int rightVal) {
	XINPUT_VIBRATION Vibration;

	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	XInputSetState(_controllerNum, &Vibration);
}