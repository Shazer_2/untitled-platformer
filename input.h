#ifndef INPUT_H
#define INPUT_H

#include "SDL.h"

class Input {
public:
	SDL_Event event;

	Input::Input();
	Input::Input(SDL_Event);
	int get_key_pressed();
	int get_key_released();
	int get_event_type();
};

#endif