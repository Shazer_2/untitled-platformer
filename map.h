#ifndef MAP_H
	#define MAP_H

#include <string>
#include <iostream>
#include <vector>
#include <tuple>

#include "block.h"

using namespace std;

class Map {
public:
	string map;
	vector<string> block_positions;

	Map(string);
	Map();
	vector<Block> get_blocks();
	tuple<bool, int, int> is_collision(int, int);
};

#endif