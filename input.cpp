#include "input.h"

Input::Input(SDL_Event _event) {
	event = _event;
}

int Input::get_key_pressed() {
	if (event.type == SDL_KEYDOWN) {
		return event.key.keysym.sym;
	}
}

int Input::get_key_released() {
	if (event.type == SDL_KEYUP) {
		return event.key.keysym.sym;
	}
}

int Input::get_event_type() {
	return event.type;
}