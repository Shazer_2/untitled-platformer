#include "SDL.h"
#include "map.h"
#include "input.h"
#include "CXBOXController.h"

class Player {
public:
	float x, y;
	float vy, vx;
	float acc;
	Map map;

	int w, h;
	int maxspeed;

	bool jumping;
	bool double_jumping;
	bool move_l, move_r;

	CXBOXController* controller;

	Player(float, float, int, int);
	void draw(SDL_Surface*, Uint32);
	void jump();
	void double_jump();
	void accelerate();
	void fall();
	void move();
	void update(SDL_Event);
	void move_left();
	void move_right();
	void check_all_collisions();
};