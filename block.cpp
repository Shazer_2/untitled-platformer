#include "block.h"
#include "SDL.h"

Block::Block(int _x, int _y, int _w, int _h) {
	x = _x;
	y = _y;
	w = _w;
	h = _h;
}

void Block::draw(SDL_Surface* screen, int x, int y, Uint32 color) {
	SDL_Rect block_rect = {x, y, w, h};
	SDL_FillRect(screen, &block_rect, color);
}

void Block::draw(SDL_Surface* screen, int x, int y, SDL_Surface* image) {
	SDL_Rect block_rect = {x, y, w, h};
	SDL_BlitSurface(image, &block_rect, screen, NULL);
}